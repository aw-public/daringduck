#!/usr/bin/env python3
from fastapi import FastAPI

PORT = 8080
BIND_ADDRESS = "127.0.0.1"

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello from Daring Duck!"}


def main() -> None:
    import uvicorn  # type: ignore

    uvicorn.run(app, host=BIND_ADDRESS, port=PORT)


if __name__ == "__main__":
    main()
