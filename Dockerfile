FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

ENV PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1 \
    POETRY_NO_INTERACTION=1 \
    POETRY_HOME="/opt/poetry" \
    PORT=8080 \
    PATH="/opt/poetry/bin/:$PATH"

WORKDIR /app

# Install poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python

# Install requirements
COPY pyproject.toml poetry.lock ./
RUN poetry install --no-dev

# Copy source
COPY ./daringduck .
