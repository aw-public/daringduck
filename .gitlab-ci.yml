image: python:3.8

stages:
  - test
  - build

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  POETRY_HOME: "/opt/poetry"
  POETRY: "${POETRY_HOME}/bin/poetry"
  PROJECT_NAME: "daringduck"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache/pip
    - venv/

before_script:
  - python -V  # Print out python version for debugging
  - curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python  # Install poetry for dependency management
  - ${POETRY} install --no-root --no-interaction --no-ansi

black:
  stage: test
  interruptible: true
  script:
    - ${POETRY} run black --check .

flake8:
  stage: test
  interruptible: true
  script:
    - ${POETRY} run flake8 ${PROJECT_NAME}

mypy:
  stage: test
  interruptible: true
  script:
    - ${POETRY} run mypy ${PROJECT_NAME}

pytest:
  stage: test
  interruptible: true
  script:
    - ${POETRY} run pytest --cov=${PROJECT_NAME} --cov-config=pyproject.toml --cov-report xml tests/
  artifacts:
    reports:
      cobertura: coverage.xml

build:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - /kaniko/executor --no-push --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG

pages:
  interruptible: true
  script:
    - cd docs; ${POETRY} run make html
    - mv _build/html/ ../public/
  artifacts:
    paths:
      - public
  only:
    - master
